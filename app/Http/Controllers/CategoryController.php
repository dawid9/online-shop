<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function show(){
        return view('category.list')->with(['categories' => Category::all()]);
    }
    public function edit($id, Category $category)
    {
        $data = Category::findOrFail($id);
        return view('category.edit')->with(['category' => $data]);
    }

    public function store(){



    }

    public function delete()
    {

    }
}
