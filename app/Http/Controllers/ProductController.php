<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;


class ProductController extends Controller
{

    public function index()
    {
//        $products = DB::table($this->db_name)->get(); --> to zwróci tablice
//        $products = Products::get();  --> to zwróci obiekty
        $products = Products::all();
        $pageTitle = "Lista produktów";
        return view('products.index')->with(['products'=> $products ]);
    }


    public function create()
    {
        return view('products.create');
    }


    public function store()
    {

    }

    public function show(int $product_id)
    {
        $product = Products::findOrFail($product_id);
        $pageTitle = "";
        return "Twój produkt to: $product_id";
    }

    public function edit(int $product_id)
    {

    }

    public function update(int $product_id)
    {

    }

    public function destroy(int $product_id)
    {

    }

}
