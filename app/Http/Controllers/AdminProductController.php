<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;

class AdminProductController extends Controller
{
    public function show()
    {
        return view('admin.products.list')->with(['products' => Products::all()]);
    }

    public function add(){
        return view('admin.products.add')->with(['categories' => Category::all()]);
    }

    public function store(Request $request){

        $validate = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'stock' => 'required',
            'price' => 'required',
            'status' => 'required',
        ]);

        $product = Products::create($request->all());
        return $product;
    }
}
