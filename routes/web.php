<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 *  Welcome page
 */
Route::get('/', 'MainController@index')->name('main');


/*
 *  Routing for products
 */
Route::prefix('products')->group(function (){
    // Display all products
    Route::get('/', 'ProductController@index')->name('products.index');

    // Send POST request with new product to DB
    Route::post('/', 'ProductController@store')->name('products.store');

    // Get specify product by ID
    Route::get('/{product_id}', 'ProductController@show')->name('products.show');

    // Edit specify product by ID
    Route::get('/{product_id}/edit', 'ProductController@edit')->name('products.edit');

    //
    Route::match(['put', 'patch'],'/{product_id}', 'ProductController@update')->name('products.update');

    // Delete specify product
    Route::delete('/{product_id}', 'ProductController@destroy')->name('product.destroy');
});

/*
 * Routing for admin panel
 */
Route::prefix('admin')->group(function(){
    // Display admin dashboard
    Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');


    Route::get('/product', 'AdminProductController@show')->name('admin.product.show');
    Route::get('/product/add', 'AdminProductController@add')->name('admin.product.add');
    Route::post('/product/add', 'AdminProductController@store')->name('admin.product.store');

    Route::get('/category', 'CategoryController@show')->name('admin.category.show');
    Route::get('/category/edit/{id}', 'CategoryController@edit')->name('admin.category.edit');Route::get('/category/edit/store/{id}', 'CategoryController@store')->name('admin.category.store');
});




