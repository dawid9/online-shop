@extends('admin.layout.header')
@section('content')
    <div class="container">

        <div class="row justify-content-center pt-2">
            <div class="col-12 text-center">
                <h3>Dostępne produkty</h3>
                <hr>
            </div>
        </div>

        @if($products->isEmpty())
            <p class="text-center">Brak produktów do wyświeltenia</p>
        @else
            <div class="row justify-content-center">
                <div class="col-12">
                    @foreach($products as $product)

                    @endforeach
                </div>
            </div>
        @endif

    </div>

@endsection
