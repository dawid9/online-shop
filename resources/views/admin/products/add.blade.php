@extends('admin.layout.header')
@section('content')
    <div class="container">

        <div class="row justify-content-center pt-2">
            <div class="col-12 text-center">
                <h3>Dodaj produkt </h3>
                <hr>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-6">
                <form action="{{route('admin.product.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Nazwa</label>
                        <input type="text" class="form-control" name="title" id="title" aria-describedby="title">
                    </div>

                    <div class="form-group">
                        <label for="description">Opis</label>
                        <textarea class="form-control" name="description" id="description" cols="10" rows="5"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="stock">Stan magazynowy</label>
                        <input type="text" class="form-control" name="stock" id="stock" aria-describedby="title">
                    </div>

                    <div class="form-group">
                        <label for="price">Cena</label>
                        <input type="text" class="form-control" name="price" id="price" aria-describedby="title">
                    </div>

                    <div class="form-group">
                        <label for="category_id">Kategoria</label>
                        <select class="form-control" name="category_id" id="category_id">
                        @if($categories->isNotEmpty())
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        @else
                            <option>Brak</option>
                        @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="available">Dostępny</option>
                            <option value="unavailable">Niedostępny</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Dodaj</button>
                </form>
            </div>
        </div>

    </div>

@endsection

