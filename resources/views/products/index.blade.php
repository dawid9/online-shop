@extends('layout.header')
@section('pageTitle', 'Lista produktów')
@section('content')
    <div class="container">

        <div class="row justify-content-center pt-2">
            <div class="col-12 text-center">
                <h1>Lista produktów</h1>
                <hr>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Zdjęcie</th>
                        <th scope="col">Nazwa</th>
                        <th>Dostępność</th>
                        <th>Cena</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        @if($product->status == 'available')
                            <tr>
                                <td>Zdjęcie</td>
                                <td>
                                    <b><a class="text-dark" href="{{ route('products.show', $product->id)}}">{{$product->title}}</a></b> <br>
                                    {{$product->description}}
                                </td>
                                <td>
                                    @if($product->stock <=10)
                                        <p class="text-danger">Ostatnie sztuki!</p>
                                    @elseif($product->stock > 10 && $product->stock < 20)
                                        <p class="text-warning">Mała ilość</p>
                                    @elseif($product->stock > 20)
                                        <p class="text-success">Duża ilość</p>
                                    @endif
                                </td>
                                <td>{{$product->price}} zł</td>
                            </tr>
                        @else
                            <tr>
                                <td>Zdjęcie</td>
                                <td>
                                    <b class="text-muted">{{$product->title}}</b> <br>
                                    {{$product->description}}
                                </td>
                                <td>
                                    <p class="text-secondary">Brak</p>
                                </td>
                                <td>{{$product->price}} zł</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
