@extends('admin.layout.header')
@section('content')
    <div class="container">

        <div class="row justify-content-center pt-2">
            <div class="col-12 text-center">
                <h3>Dostępne kategorie</h3>
                <hr>
            </div>
        </div>
        @if($categories->isEmpty())
            <p>Brak kategorii</p>
        @else
            <div class="row justify-content-center">
                <div class="col-6">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nazwa</th>
                                <th>Edytuj</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-info dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Edytuj
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{route('admin.category.edit', $category)}}"> <i class="fas fa-edit"></i> Edytuj </a>
                                            <a class="dropdown-item" data-delete="{{$category->id}}" href="#"> <i class="fas fa-trash-alt"></i> Usuń</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>

@endsection


@push('scripts')
    <script>
        {{--console.log('elo')--}}
        {{--$('.dropdown-item').on('click', function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    let url = $(this).attr('data-edit')--}}

        {{--    $.ajax({--}}
        {{--        type: 'GET',--}}
        {{--        url : {{'admin.category.edit'}}--}}

        {{--    })--}}
        {{--})--}}
    </script>
@endpush
