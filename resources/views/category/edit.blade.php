@extends('admin.layout.header')
@section('content')
    <div class="container">

        <div class="row justify-content-center pt-2">
            <div class="col-12 text-center">
                <h3>Edytuj kategorię</h3>
                <hr>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-6">
                <form action="" method="GET">
                    @csrf
                    <div class="form-group">
                        <label for="category_name">Nazwa kategorii </label>
                        <input type="text" class="form-control" name="category_name" value="{{$category->name}}" id="category_name" aria-describedby="emailHelp">
                    </div>
                    <button type="submit" id="save" class="btn btn-primary">Zapisz</button>
                    <button type="submit" id="cancel" class="btn btn-primary">Anuluj</button>
                </form>
            </div>
        </div>

    </div>

    @push('scripts')
        <script>
            $('#cancel').on('click', function (e) {
                e.preventDefault()
                location.href = '{{route('admin.category.show')}}';
            });
        </script>
    @endpush
@endsection
