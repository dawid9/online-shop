<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 *  Welcome page
 */
Route::get('/', 'MainController@index')->name('main');


/*
 *  Routing for products
 */
Route::prefix('products')->group(function (){
    // Display all products
    Route::get('/', 'ProductController@index')->name('products.index');

    // Get form for creating new products
    Route::get('/create', 'ProductController@create')->name('products.create');

    // Send POST request with new product to DB
    Route::post('/', 'ProductController@store')->name('products.store');

    // Get specify product by ID
    Route::get('/{product_id}', 'ProductController@show')->name('products.show');

    // Edit specify product by ID
    Route::get('/{product_id}/edit', 'ProductController@edit')->name('products.edit');

    //
    Route::match(['put', 'patch'],'/{product_id}', 'ProductController@update')->name('products.update');

    // Delete specify product
    Route::delete('/{product_id}', 'ProductController@destroy')->name('product.destroy');
});





