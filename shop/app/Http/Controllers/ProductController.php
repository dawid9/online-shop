<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    /*
     *  Table name
     */
    private $db_name = 'products';


    public function index()
    {
//        $products = DB::table($this->db_name)->get(); --> to zwróci tablice
//        $products = Products::get();  --> to zwróci obiekty
        $products = Products::all();
        $pageTitle = "Lista produktów";
        return view('products.index' , ['pageTitle' => $pageTitle, 'products'=> $products ]);
    }


    public function create()
    {
        return "Dodaj produkt";
    }


    public function store()
    {

    }


    public function show(int $product_id)
    {
//        $product = DB::table($this->db_name)->find($product_id);
//        $product = Products::where('id', $product_id)->first();
        $product = Products::findOrFail($product_id);
        dd($product);
        $pageTitle = "";
        return "Twój produkt to: $product_id";
    }

    public function edit(int $product_id)
    {

    }

    public function update(int $product_id)
    {

    }

    public function destroy(int $product_id)
    {

    }
}
