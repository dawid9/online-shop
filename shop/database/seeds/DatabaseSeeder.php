<?php

use Illuminate\Database\Seeder;
use App\Products;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $products = factory(Products::class, 50)->create();

        /*
         * php artisan db:seed
         */
    }
}
