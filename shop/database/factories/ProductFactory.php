<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
//    $faker = ['pl_PL'];

    return [
        'title'         => $faker->sentence(2),
        'description'   => $faker->paragraph(2),
        'price'         => $faker->randomFloat(2,3, 1000),
        'stock'         => $faker->numberBetween(1,100),
        'status'        => $faker->randomElement(['available', 'unavailable'])
    ];

    /*
     * @use php artisan tinker
     * @see factory(App\Products::class, 10)->create()
     */
});
